from .base import BaseRepository
from db.todo_items import todo_items
from db.todos import todos
from schemas.todo_item import TodoItem, BaseTodoItem
from schemas.user import User
from fastapi import HTTPException, status
from sqlalchemy import select
from settings import settings


class TodoItemRepository(BaseRepository):
    async def create(self, todo_item: BaseTodoItem) -> TodoItem:
        query = todo_items.insert().values(**todo_item.dict())
        new_todo_item=TodoItem.parse_obj(todo_item)
        new_todo_item.id = await self.database.execute(query = query)
        return new_todo_item

    async def update(self, id: int, todo_item: BaseTodoItem) -> TodoItem:
        query = todo_items.update().values(**todo_item.dict()).where(todo_items.c.id == id)
        await self.database.execute(query = query)
        return todo_item

    async def delete(self, id: int) -> TodoItem:
        query = todo_items.delete().where(todo_items.c.id == id)
        if await self.database.execute(query = query):
            return {"success": True}

    async def get_all(self, limit: int = 100, skip: int = 0) -> list[TodoItem]:
        query = todo_items.select().limit(limit).offset(skip)
        return await self.database.fetch_all(query = query)

    async def get_by_todo_id(self, todo_id: int) -> TodoItem:
        query = todo_items.select().where(todo_items.c.todo_id == todo_id)
        return await self.database.fetch_all(query = query)

    async def get_by_id(self, id: int, user: User) -> TodoItem:
        # query = todo_items.select().where(todo_items.c.id == id)
        query = select([
            todo_items.c.id,
            todo_items.c.todo_id,
            todo_items.c.title,
            todo_items.c.description,
            todo_items.c.done,
            todos.c.user_id]).select_from(todo_items.join(todos)).where(todo_items.c.id == id)
        todo_item = await self.database.fetch_one(query = query)

        if (settings.database_type == 'sqlite'):
            if todo_item is None or todo_item.id != id:
                raise HTTPException(status_code = status.HTTP_404_NOT_FOUND, detail = "Item not found")
            if todo_item is None or todo_item.user_id != user.id:
                raise HTTPException(status_code = status.HTTP_403_FORBIDDEN, detail = "Access denied")
            return TodoItem.parse_obj(todo_item)

        elif (settings.database_type == 'postgresql'):
            if todo_item is None or todo_item.get('id') != id:
                raise HTTPException(status_code = status.HTTP_404_NOT_FOUND, detail = "Item not found")
            if todo_item is None or todo_item['user_id'] != user.id:
                raise HTTPException(status_code = status.HTTP_403_FORBIDDEN, detail = "Access denied")
            return TodoItem(**todo_item)

