from typing import Optional
from db.users import users
from schemas.user import User, UserCreate, BaseUser, DbUser
from .base import BaseRepository
from services.security import hash_password

class UserRepository(BaseRepository):
    async def get_by_id(self, id: int) -> Optional[User]:
        query = users.select().where(users.c.id==id)
        user = await self.database.fetch_one(query)
        if user is None:
            return None
        return User.parse_obj(user)

    async def create(self, user: BaseUser) -> User:
        values=user.dict()
        values['hashed_password']=hash_password(values['password'])
        create_user = UserCreate(**values)
        new_user = User.parse_obj(user)
        query = users.insert().values(**create_user.dict())
        new_user.id = await self.database.execute(query)
        return new_user

    async def update(self, id: int, user: User) -> User:
        values = user.dict()
        values['hashed_password'] = hash_password(values['password'])
        user_update = UserCreate(**values)
        query = users.update().where(users.c.id==id).values(**user_update.dict())
        await self.database.execute(query)
        updated_user = User.parse_obj(user)
        updated_user.id = id
        return updated_user

    async def get_by_email(self, email: str) -> DbUser:
        query = users.select().where(users.c.email==email)
        user = await self.database.fetch_one(query)
        if user is None:
            return None
        return DbUser.parse_obj(user)
