from .base import BaseRepository
from db.todos import todos
from schemas.todo import Todo, BaseTodo, TodoCreate
from schemas.user import User
from fastapi import HTTPException, status
from settings import settings

class TodoRepository(BaseRepository):
    async def get_all(self, user: User, limit: int = 100, skip: int = 0) -> list[Todo]:
        query = todos.select().limit(limit).where(todos.c.user_id == user.id).offset(skip)
        all_todos = await self.database.fetch_all(query = query)
        return [Todo.parse_obj(todo) for todo in all_todos]

    async def create(self, todo: BaseTodo, user: User) -> Todo:
        todo_create = TodoCreate(**{**todo.dict(), "user_id": user.id})
        query = todos.insert().values(**todo_create.dict())
        new_todo = Todo.parse_obj(todo_create)
        new_todo.id = await self.database.execute(query = query)
        return new_todo

    async def update(self, id: int, todo: BaseTodo, user: User) -> Todo:
        query = todos.update().values(**todo.dict()).where(todos.c.id == id)
        await self.database.execute(query = query)
        todo_update = Todo(**{**todo.dict(), "user_id": user.id, "id": id})
        return todo_update

    async def delete(self, id: int):
        query = todos.delete().where(todos.c.id == id)
        if await self.database.execute(query = query):
            return {"success": True}

    async def get_by_id(self, id: int, user: User) -> Todo:
        query = todos.select().where(todos.c.id == id)
        todo = await self.database.fetch_one(query = query)

        if(settings.database_type=='sqlite'):
            if todo is None or todo.id != id:
                raise HTTPException(status_code = status.HTTP_404_NOT_FOUND, detail = "Todo not found")
            if todo is None or todo.user_id != user.id:
                raise HTTPException(status_code = status.HTTP_403_FORBIDDEN, detail = "Access denied")
            return Todo.parse_obj(todo)

        elif(settings.database_type=='postgresql'):
            if todo is None or todo.get('id') != id:
                raise HTTPException(status_code = status.HTTP_404_NOT_FOUND, detail = "Todo not found")
            if todo is None or todo['user_id'] != user.id:
                raise HTTPException(status_code = status.HTTP_403_FORBIDDEN, detail = "Access denied")
            return Todo(**todo)