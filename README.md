# API for TODO application

#### Functionality:
1. User registration, JWT-based authorization
2. TODO CRUDs
3. TODO items CRUDs
4. Divided permissions
5. Tests
6. Docker compose config
7. Alembic automatic migrations

#### How to run:
1. Clone this git
2. Install docker and docker-compose
3. Enter git directory and run
`docker-compose up --build -d`