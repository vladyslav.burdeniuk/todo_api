from db.base import database
from repositories.todo import TodoRepository
from repositories.todo_items import TodoItemRepository
from repositories.user import UserRepository
from fastapi.security import OAuth2PasswordBearer
from fastapi import Depends, status, HTTPException
from services.security import JWTBearer, decode_access_token
from schemas.user import User

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")


def get_todo_repository() -> TodoRepository:
    return TodoRepository(database)


def get_todo_item_repository() -> TodoItemRepository:
    return TodoItemRepository(database)


def get_user_repository() -> UserRepository:
    return UserRepository(database)


async def get_current_user(
    users: UserRepository = Depends(get_user_repository),
    token: str = Depends(JWTBearer()),
) -> User:
    cred_exception = HTTPException(
        status_code=status.HTTP_403_FORBIDDEN, detail="Credentials are not valid"
    )
    payload = decode_access_token(token)
    if payload is None:
        raise cred_exception
    email: str = payload.get("sub")
    if email is None:
        raise cred_exception
    user = await users.get_by_email(email=email)
    if user is None:
        raise cred_exception
    return user
