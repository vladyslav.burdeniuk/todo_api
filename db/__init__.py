from db.base import engine, metadata
from db.todos import todos
from db.todo_items import todo_items
from db.users import users

metadata.create_all(bind=engine)
