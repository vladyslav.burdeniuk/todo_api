import sqlalchemy

from db.base import metadata

todo_items = sqlalchemy.Table(
    "todo_items",
    metadata,
    sqlalchemy.Column("id", sqlalchemy.Integer, primary_key=True, autoincrement=True, unique=True),
    sqlalchemy.Column("title", sqlalchemy.String),
    sqlalchemy.Column("description", sqlalchemy.String),
    sqlalchemy.Column("done", sqlalchemy.Boolean, default = False),
    sqlalchemy.Column('todo_id', sqlalchemy.Integer, sqlalchemy.ForeignKey('todos.id')),
)