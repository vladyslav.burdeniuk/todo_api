FROM python:3.9
WORKDIR /var/todo_api_application
COPY . .
RUN pip install --upgrade pip && pip install --no-cache-dir -r requirements.txt
ENV PYTHONUNBUFFERED 1
CMD uvicorn main:app --reload --host 0.0.0.0 --port 8000