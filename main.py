import uvicorn
from settings import settings
from fastapi import FastAPI
from endpoints import todo, todo_item, users, auth
from db.base import database


app = FastAPI(title="TODO API application")
app.include_router(todo.router, prefix="/todos", tags=["todos"])
app.include_router(todo_item.router, prefix="/todo_items", tags=["todo_items"])
app.include_router(users.router, prefix="/users", tags=["users"])
app.include_router(auth.router, prefix="/auth", tags=["auth"])


@app.on_event("startup")
async def startup():
    await database.connect()


@app.on_event("shutdown")
async def shutdown():
    await database.disconnect()


if __name__ == "__main__":
    uvicorn.run(
        "main:app",
        host=settings.server_host,
        port=settings.server_port,
        reload=True,
    )