import os
from fastapi.testclient import TestClient

os.environ["DATABASE_URL"] = "sqlite:///./test_db.sqlite3"
from main import app  # noqa

client = TestClient(app)

def test_setup(token):
    # todo: move to fixture
    client.post("/users/", json = {"email": "example@example.com", "password": "example123"})
    response = client.post("/auth/", json = {"email": "example@example.com", "password": "example123"})
    token.token = response.json()["access_token"]
    client.post("/todos/", json = {"title": "Title"}, headers = {"Authorization": f"Bearer {token.token}"})

def test_post_todo_items(token):
    response = client.post(
        "/todo_items/",
        json={"todo_id": 1, "title": "Title", "description": "Description"},
        headers={"Authorization": f"Bearer {token.token}"})
    assert response.status_code == 201

def test_get_todo_items(token):
    response = client.get("/todo_items/1", headers={"Authorization": f"Bearer {token.token}"})
    assert response.json()["title"]=="Title"
    assert response.status_code == 200

def test_put_todo_items(token):
    response = client.put(
        "/todo_items/1",
        json={"todo_id": 1, "title": "NewTitle", "description": "Description"},
        headers={"Authorization": f"Bearer {token.token}"})
    assert response.status_code == 201

def test_update_result_todo_items(token):
    response = client.get("/todo_items/1", headers={"Authorization": f"Bearer {token.token}"})
    assert response.json()["title"]=="NewTitle"
    assert response.status_code == 200

def test_delete_todo_items(token):
    response = client.delete("/todo_items/1", headers={"Authorization": f"Bearer {token.token}"})
    assert response.status_code == 204

def test_delete_recheck_todo_items(token):
    response = client.delete("/todo_items/1", headers={"Authorization": f"Bearer {token.token}"})
    assert response.status_code == 404
