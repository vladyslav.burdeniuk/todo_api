import os
import pytest

class Token:
    def __init__(self):
        self.token=None


@pytest.fixture(scope='module')
def token() -> Token:
    token = Token()
    token.token = 0
    return token

@pytest.fixture(scope='session', autouse=True)
def setup_and_teardown():
    yield
    os.remove('./test_db.sqlite3')