import os
from fastapi.testclient import TestClient

os.environ["DATABASE_URL"] = "sqlite:///./test_db.sqlite3"
from main import app  # noqa

client = TestClient(app)

def test_setup(token):
    # todo: move to fixture
    client.post("/users/", json = {"email": "example@example.com", "password": "example123"})
    response = client.post("/auth/", json = {"email": "example@example.com", "password": "example123"})
    token.token = response.json()["access_token"]

def test_post_todos(token):
    response = client.post("/todos/", json={"title": "Title"}, headers={"Authorization": f"Bearer {token.token}"})
    assert response.status_code == 201

def test_get_todos(token):
    response = client.get("/todos/1", headers={"Authorization": f"Bearer {token.token}"})
    assert response.json()["title"]=="Title"
    assert response.status_code == 200

def test_put_todos(token):
    response = client.put("/todos/1", json={"title": "NewTitle"}, headers={"Authorization": f"Bearer {token.token}"})
    assert response.status_code == 201

def test_update_result_todos(token):
    response = client.get("/todos/1", headers={"Authorization": f"Bearer {token.token}"})
    assert response.json()["title"]=="NewTitle"
    assert response.status_code == 200

def test_delete_todos(token):
    response = client.delete("/todos/1", headers={"Authorization": f"Bearer {token.token}"})
    assert response.status_code == 204

def test_delete_recheck_todos(token):
    response = client.delete("/todos/1", headers={"Authorization": f"Bearer {token.token}"})
    assert response.status_code == 404
