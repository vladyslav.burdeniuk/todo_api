import os
from fastapi.testclient import TestClient

os.environ["DATABASE_URL"] = "sqlite:///./test_db.sqlite3"
from main import app  # noqa

client = TestClient(app)


def test_get_todos_require_authorization():
    response = client.get("/todos/1")
    assert response.status_code == 403


def test_post_todos_require_authorization():
    response = client.post("/todos/")
    assert response.status_code == 403


def test_put_todos_require_authorization():
    response = client.put("/todos/1")
    assert response.status_code == 403


def test_delete_todos_require_authorization():
    response = client.delete("/todos/1")
    assert response.status_code == 403


def test_get_todo_items_require_authorization():
    response = client.get("/todo_items/1")
    assert response.status_code == 403


def test_post_todo_items_require_authorization():
    response = client.post("/todo_items/")
    assert response.status_code == 403


def test_put_todo_items_require_authorization():
    response = client.put("/todo_items/1")
    assert response.status_code == 403


def test_delete_todo_items_require_authorization():
    response = client.delete("/todo_items/1")
    assert response.status_code == 403

def test_signup_short_password():
    response = client.post(
        "/users/", json={"email": "example@example.com", "password": "example"}
    )
    assert (
        response.json()["detail"][0]["msg"]
        == "ensure this value has at least 8 characters"
    )


def test_signup():
    response = client.post(
        "/users/", json={"email": "example@example.com", "password": "example123"}
    )
    assert response.status_code == 200


def test_user_update_require_authorization():
    response = client.put(
        "/users/", json={"email": "example1@example.com", "password": "example1234"}
    )
    assert response.status_code == 403


def test_login(token):
    response = client.post(
        "/auth/", json={"email": "example@example.com", "password": "example123"}
    )
    token.token = response.json()["access_token"]
    assert response.status_code == 200


def test_user_update(token):
    response = client.put(
        "/users/",
        json={"email": "example@example.com", "password": "example123"},
        headers={"Authorization": f"Bearer {token.token}"},
    )
    assert response.status_code == 200
