from pydantic import BaseModel, EmailStr, constr


class BaseUser(BaseModel):
    email: EmailStr
    password: constr(min_length = 8)


class UserCreate(BaseModel):
    email: EmailStr
    hashed_password: str


class User(BaseModel):
    id: int = None
    email: EmailStr


class DbUser(BaseModel):
    id: int = None
    email: EmailStr
    hashed_password: str