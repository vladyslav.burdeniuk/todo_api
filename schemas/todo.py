from pydantic import BaseModel


class BaseTodo(BaseModel):
    title: str


class TodoCreate(BaseModel):
    title: str
    user_id: int


class Todo(BaseModel):
    id: int = None
    title: str
    user_id: int