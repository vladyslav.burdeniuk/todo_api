from pydantic import BaseModel


class BaseTodoItem(BaseModel):
    todo_id: int
    title: str
    description: str
    done: bool = False


class TodoItem(BaseTodoItem):
    id: int = None