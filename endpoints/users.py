from fastapi import APIRouter, Depends, HTTPException, status
from repositories.user import UserRepository
from schemas.user import User, BaseUser
from services.depends import get_user_repository, get_current_user


router = APIRouter()

@router.post("/", response_model=User)
async def create_user(
    user: BaseUser,
    users: UserRepository = Depends(get_user_repository)):
    return await users.create(user=user)

@router.put("/", response_model=User)
async def update_user(
    user: BaseUser,
    users: UserRepository = Depends(get_user_repository),
    current_user: User = Depends(get_current_user)
):
    if not await users.get_by_id(id=current_user.id):
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Not found user")
    return await users.update(id=current_user.id, user=user)