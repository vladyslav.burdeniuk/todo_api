from schemas.todo_item import TodoItem, BaseTodoItem

from fastapi import APIRouter, Depends
from schemas.user import User
from services.depends import get_todo_item_repository, get_current_user, get_todo_repository
from repositories.todo_items import TodoItemRepository
from repositories.todo import TodoRepository

router = APIRouter()


@router.get("/by_todo_id/{todo_id}", response_model=list[TodoItem])
async def todo_item_get_by_todo_id(
    todo_id: int,
    todo_item_repository: TodoItemRepository = Depends(get_todo_item_repository),
    todo_repository: TodoRepository = Depends(get_todo_repository),
    user: User = Depends(get_current_user),
):
    await todo_repository.get_by_id(id = id, user = user)
    return await todo_item_repository.get_by_todo_id(todo_id=todo_id)


@router.get("/{id}", response_model=TodoItem)
async def todo_item_get(
    id: int,
    todo_item_repository: TodoItemRepository = Depends(get_todo_item_repository),
    user: User = Depends(get_current_user),
):
    return await todo_item_repository.get_by_id(id=id, user=user)


@router.post("/", status_code=201, response_model=TodoItem)
async def todo_item_create(
    todo_item: BaseTodoItem,
    todo_item_repository: TodoItemRepository = Depends(get_todo_item_repository),
    user: User = Depends(get_current_user) # noqa
):
    return await todo_item_repository.create(todo_item=todo_item)


@router.put("/{id}", status_code=201, response_model=TodoItem)
async def todo_item_update(
    id: int,
    todo_item: BaseTodoItem,
    todo_item_repository: TodoItemRepository = Depends(get_todo_item_repository),
    user: User = Depends(get_current_user),
):
    await todo_item_repository.get_by_id(id = id, user = user)
    return await todo_item_repository.update(id = id, todo_item = todo_item)


@router.delete("/{id}", status_code=204)
async def todo_item_delete(
    id: int,
    todo_item_repository: TodoItemRepository = Depends(get_todo_item_repository),
    user: User = Depends(get_current_user),
):
    await todo_item_repository.get_by_id(id = id, user = user)
    return await todo_item_repository.delete(id=id)