from schemas.todo import Todo, BaseTodo

from fastapi import APIRouter, Depends, HTTPException, status
from schemas.user import User
from services.depends import get_todo_repository, get_current_user
from repositories.todo import TodoRepository
router = APIRouter()


@router.get("/", response_model=list[Todo])
async def todo_list(
    limit: int = 100,
    skip: int = 0,
    user: User = Depends(get_current_user),
    todo_repository: TodoRepository = Depends(get_todo_repository),
):
    return await todo_repository.get_all(limit=limit, skip=skip, user=user)


@router.get("/{id}", response_model=Todo)
async def todo_get(
    id: int,
    todo_repository: TodoRepository = Depends(get_todo_repository),
    user: User = Depends(get_current_user),
):
    await todo_repository.get_by_id(id = id, user = user)
    return await todo_repository.get_by_id(id=id, user=user)


@router.post("/", status_code=201, response_model=Todo)
async def todo_create(
    todo: BaseTodo,
    todo_repository: TodoRepository = Depends(get_todo_repository),
    user: User = Depends(get_current_user),
):
    return await todo_repository.create(todo=todo, user=user)


@router.put("/{id}", status_code=201, response_model=Todo)
async def todo_update(
    id: int,
    todo_income: BaseTodo,
    todo_repository: TodoRepository = Depends(get_todo_repository),
    user: User = Depends(get_current_user),
):
    await todo_repository.get_by_id(id = id, user = user)
    return await todo_repository.update(id = id, todo = todo_income, user=user)


@router.delete("/{id}", status_code=204)
async def todo_delete(
    id: int,
    todo_repository: TodoRepository = Depends(get_todo_repository),
    user: User = Depends(get_current_user),
):
    await todo_repository.get_by_id(id = id, user = user)
    return await todo_repository.delete(id=id)